<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 13:46
 */

namespace App\Host;

use App\HTTP\Request;

class Host implements HostInterface
{
    private $hostname;

    /**
     * Host constructor.
     *
     * @param string $hostname
     */
    public function __construct(string $hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return float
     */
    public function getLoad(): float
    {
        return round(mt_rand() / mt_getrandmax(), 2);
    }

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request): void
    {
        echo 'Make request to: ' . $this->getHostname();
    }
}