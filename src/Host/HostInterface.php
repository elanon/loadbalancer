<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 13:46
 */

namespace App\Host;

use App\HTTP\Request;

interface HostInterface
{
    /**
     * HostInterface constructor.
     *
     * @param string $hostname
     */
    public function __construct(string $hostname);

    /**
     * @return string
     */
    public function getHostname() : string;

    /**
     * @return float
     */
    public function getLoad() : float;

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request) : void;
}