<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 13:12
 */

namespace App\AutoBalance;

use App\BalanceAlgorithm\BalanceAlgorithmInterface;
use App\HTTP\Request;

interface AutoBalancerInterface
{
    /**
     * AutoBalancerInterface constructor.
     *
     * @param array                     $hosts
     * @param BalanceAlgorithmInterface $balanceAlgorithm
     */
    public function __construct(array $hosts, BalanceAlgorithmInterface $balanceAlgorithm);

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request) : void;
}