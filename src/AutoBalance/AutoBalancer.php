<?php

namespace App\AutoBalance;

use App\BalanceAlgorithm\BalanceAlgorithmInterface;
use App\HTTP\Request;

class AutoBalancer implements AutoBalancerInterface
{
    private $hosts;
    private $algorithm;

    /**
     * AutoBalancer constructor.
     *
     * @param array                     $hosts
     * @param BalanceAlgorithmInterface $balanceAlgorithm
     */
    public function __construct(array $hosts, BalanceAlgorithmInterface $balanceAlgorithm)
    {
        $this->hosts = $hosts;
        $this->algorithm = $balanceAlgorithm;
    }

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request): void
    {
        $host = $this->algorithm->calculate($this->hosts);
        $host->handleRequest($request);
    }
}