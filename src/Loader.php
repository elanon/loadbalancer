<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:04
 */

namespace App;

require_once('autoload.php');

use App\AutoBalance\AutoBalancerInterface;
use App\AutoBalance\AutoBalancer;
use App\BalanceAlgorithm\LoadCheckAlgorithm;
use App\BalanceAlgorithm\SequenceAlgorithm;
use App\Host\Host;
use App\HTTP\Request;

class Loader
{
    private $autoBalancer;

    /**
     * Loader constructor.
     *
     * @param AutoBalancerInterface $autoBalancer
     */
    public function __construct(AutoBalancerInterface $autoBalancer)
    {
        $this->autoBalancer = $autoBalancer;
    }

    /**
     * @param Request $request
     */
    public function passRequest(Request $request)
    {
        $this->autoBalancer->handleRequest($request);
    }
}


$host1 = new Host('Host.one');
$host2 = new Host('Host.two');
$host3 = new Host('Host.three');
$host4 = new Host('Host.four');
$host5 = new Host('Host.five');
$host6 = new Host('Host.six');
$host7 = new Host('Host.seven');
$host8 = new Host('Host.eight');
$hosts = [$host1, $host2, $host3, $host4, $host5, $host6, $host7, $host8];

$sequenceAlgorithm = new SequenceAlgorithm();
$loadCheckAlgorithm = new LoadCheckAlgorithm();

$sequenceBalancer = new AutoBalancer($hosts, $sequenceAlgorithm);
$loadCheckBalancer = new AutoBalancer($hosts, $loadCheckAlgorithm);

$sequenceLoader = new Loader($sequenceBalancer);
$loadLoader = new Loader($loadCheckBalancer);

echo "----- Sequence Algorithm ------- Load Algorithm\n";

$request = new Request();
for ($i = 0; $i <= 50; ++ $i) {
    $sequenceLoader->passRequest($request);
    echo ' ------ ';
    $loadLoader->passRequest($request);
    echo "\n";
    sleep(2);
}