<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:17
 */

namespace App\BalanceAlgorithm;


use App\Exception\InvalidHostException;
use App\Host\HostInterface;

class SequenceAlgorithm implements BalanceAlgorithmInterface
{

    /**
     * @param array $hosts
     *
     * @return HostInterface
     * @throws InvalidHostException
     */
    public function calculate(array &$hosts): HostInterface
    {
        $host = current($hosts);

        if (next($hosts) === false) {
            reset($hosts);
        }

        if ($host instanceof HostInterface) {
            return $host;
        } else {
            throw new InvalidHostException();
        }
    }
}