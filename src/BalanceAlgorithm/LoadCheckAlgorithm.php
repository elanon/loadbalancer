<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:17
 */

namespace App\BalanceAlgorithm;


use App\Exception\InvalidHostException;
use App\Host\HostInterface;

class LoadCheckAlgorithm implements BalanceAlgorithmInterface
{

    /**
     * @param array $hosts
     *
     * @return HostInterface
     * @throws InvalidHostException
     */
    public function calculate(array &$hosts): HostInterface
    {
        $lowestHost = null;
        foreach ($hosts as $host) {
            if (!$host instanceof HostInterface) {
                throw new InvalidHostException();
            }

            if (($load = $host->getLoad()) >= 0.75) {
                if ($lowestHost === null) {
                    $lowestHost = $host;
                } elseif ($lowestHost instanceof HostInterface && $lowestHost->getLoad() > $load) {
                    $lowestHost = $host;
                }
            } else {
                return $host;
            }
        }

        return $lowestHost;
    }
}