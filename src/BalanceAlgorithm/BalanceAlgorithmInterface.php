<?php

namespace App\BalanceAlgorithm;

use App\Host\HostInterface;

interface BalanceAlgorithmInterface
{
    /**
     * @param array $hosts
     *
     * @return HostInterface
     */
    public function calculate(array &$hosts) : HostInterface;
}