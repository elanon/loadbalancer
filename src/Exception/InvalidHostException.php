<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15.04.19
 * Time: 07:46
 */

namespace App\Exception;


use Throwable;

class InvalidHostException extends \Exception
{
    public function __construct(string $message = "Not host object passed", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}