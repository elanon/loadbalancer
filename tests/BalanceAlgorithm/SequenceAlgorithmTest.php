<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:17
 */

use PHPUnit\Framework\TestCase;
use App\Host\Host;
use App\BalanceAlgorithm\SequenceAlgorithm;
use App\Exception\InvalidHostException;

class SequenceAlgorithmTest extends TestCase
{

    /**
     * @throws InvalidHostException
     */
    public function testGoodHostPicking()
    {
        $hosts = [];
        for ($i = 0; $i < 10; ++ $i) {
            $hosts[$i] = new Host('localhost' . $i);
        }

        $algorithm = new SequenceAlgorithm();

        for ($i = 0; $i < 100; ++ $i) {
            $host = $algorithm->calculate($hosts);
            $this->assertInstanceOf(Host::class, $host);
            $this->assertEquals($hosts[$i % 10], $host);
        }
    }

    /**
     * @throws InvalidHostException
     */
    public function testNotHostPassed()
    {
        $hosts = [];
        $hosts[0] = new \DateTime();
        $algorithm = new SequenceAlgorithm();

        $this->expectException(InvalidHostException::class);
        $host = $algorithm->calculate($hosts);
    }
}