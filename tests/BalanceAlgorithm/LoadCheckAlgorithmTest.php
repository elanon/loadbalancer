<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:17
 */

use PHPUnit\Framework\TestCase;
use App\BalanceAlgorithm\LoadCheckAlgorithm;
use App\Exception\InvalidHostException;

class LoadCheckAlgorithmTest extends TestCase
{

    /**
     * @throws InvalidHostException
     * @throws ReflectionException
     */
    public function testGetLowestLoadAlgorithmButUpperLimit()
    {
        $host1 = $this->createMock(\App\Host\Host::class);
        $host1->method("getLoad")->willReturn('0.77');
        $host2 = $this->createMock(\App\Host\Host::class);
        $host2->method("getLoad")->willReturn('0.89');
        $host3 = $this->createMock(\App\Host\Host::class);
        $host3->method("getLoad")->willReturn('0.95');
        $host4 = $this->createMock(\App\Host\Host::class);
        $host4->method("getLoad")->willReturn('0.76');
        $host5 = $this->createMock(\App\Host\Host::class);
        $host5->method("getLoad")->willReturn('0.84');
        $hosts = [$host1, $host2, $host3, $host4, $host5];

        $algorithm = new LoadCheckAlgorithm();
        $host = $algorithm->calculate($hosts);

        $this->assertEquals('0.76', $host->getLoad());
    }

    /**
     * @throws InvalidHostException
     * @throws ReflectionException
     */
    public function testGetFirstLoadUnderLimit()
    {
        $host1 = $this->createMock(\App\Host\Host::class);
        $host1->method("getLoad")->willReturn('0.77');
        $host2 = $this->createMock(\App\Host\Host::class);
        $host2->method("getLoad")->willReturn('0.89');
        $host3 = $this->createMock(\App\Host\Host::class);
        $host3->method("getLoad")->willReturn('0.95');
        $host4 = $this->createMock(\App\Host\Host::class);
        $host4->method("getLoad")->willReturn('0.76');
        $host5 = $this->createMock(\App\Host\Host::class);
        $host5->method("getLoad")->willReturn('0.84');
        $host6 = $this->createMock(\App\Host\Host::class);
        $host6->method("getLoad")->willReturn('0.22');
        $host7 = $this->createMock(\App\Host\Host::class);
        $host7->method("getLoad")->willReturn('0.10');
        $hosts = [$host1, $host2, $host3, $host4, $host5, $host6, $host6];

        $algorithm = new LoadCheckAlgorithm();
        $host = $algorithm->calculate($hosts);

        $this->assertEquals('0.22', $host->getLoad());
    }

    /**
     * @throws InvalidHostException
     */
    public function testNotHostPassed()
    {
        $hosts = [];
        $hosts[0] = new \DateTime();
        $algorithm = new LoadCheckAlgorithm();

        $this->expectException(InvalidHostException::class);
        $host = $algorithm->calculate($hosts);
    }
}