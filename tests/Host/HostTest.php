<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.19
 * Time: 14:29
 */

use PHPUnit\Framework\TestCase;
use App\Host\Host;


class HostTest extends TestCase
{
    public function testGetHostname()
    {
        $hostOne = new Host('Hostname1');
        $hostTwo = new Host('Hostname2');

        $this->assertEquals('Hostname1', $hostOne->getHostname());
        $this->assertEquals('Hostname2', $hostTwo->getHostname());
    }

    public function testMaximumLoadIsOne()
    {
        $hostOne = new Host('Hostname');

        for ($i = 0; $i<500; $i++) {
            $this->assertLessThan(1.01, $hostOne->getLoad());
        }
    }
}